#include <Arduino.h>
#include <SPI.h>
#include <MCP2515.h>

#include "config.h"
#include "DBC_Structs_EDU.h"
#include "DBC_Structs_addon.h"
#include "timer.h"



/* CAN controller object */
MCP2515 CAN_EDU(CAN_EDU_NOT_CS_PIN,CAN_EDU_RESET_PIN, CAN_EDU_NOT_INT_PIN);


void init_DIM();
void can_EDU_receive();
void send_CAN_msg(uint16_t SID,
                  uint8_t *message,
                  uint8_t DLC);

uint16_t unpack_frame(uint8_t *message,
                      uint8_t startBit,
                      uint8_t bitLength,
                      uint8_t Motorola_Intel);

int add_signal( uint8_t *message,
                int16_t value,
                uint8_t startBit,
                uint8_t bitLength,
                uint8_t Motorola_Intel);
char cmd;
int cem3_n = 0, r_or_l = 0, w = 3;
bool b = false;
float speed = 0;
uint16_t rpm;
uint16_t gear = 1;

const uint16_t speedToRPM[6] = {75, 37, 25, 19, 15,12};

Frame can_frame_power;

void cem1_send(){
  SEND_CAN_MSG(CEM_1);
  delay(1);
  ADD_CAN_SIGNAL(CEM_1, Power_Mode_UB, 0);
}

void cem2_send(){
  SEND_CAN_MSG(CEM_2);
  delay(1);
  ADD_CAN_SIGNAL(CEM_2, Engine_Speed_UB,      0); //
  ADD_CAN_SIGNAL(CEM_2, Vehicle_Speed_UB,     0); //
  ADD_CAN_SIGNAL(CEM_2, Fuel_Sensor_UB,       0); //
}

void cem3_send(){
  ADD_CAN_SIGNAL(CEM_3, Direction_Indication, 0);
  ADD_CAN_SIGNAL(CEM_3, Direction_Indication_UB, 1);
  cem3_n = (cem3_n + 1) % 5;
  if (cem3_n == 0) {
    if(r_or_l == 2) {
      ADD_CAN_SIGNAL(CEM_3, Direction_Indication,     2); // No blinker
    } else if (r_or_l == 1) {
      ADD_CAN_SIGNAL(CEM_3, Direction_Indication,     1);
    } else {
      ADD_CAN_SIGNAL(CEM_3, Direction_Indication,     0);
      ADD_CAN_SIGNAL(CEM_3, Direction_Indication_UB,  1);
    }
  }

  SEND_CAN_MSG(CEM_3);
  delay(1);
  ADD_CAN_SIGNAL(CEM_3, Direction_Indication_UB,  0); // No change
  ADD_CAN_SIGNAL(CEM_3, Main_Beam_Indication_UB,  0); // no change
  ADD_CAN_SIGNAL(CEM_3, ABS_Warning_Lamp_UB,      0); // No Change
  ADD_CAN_SIGNAL(CEM_3, Read_Message,             0); // No Change
}


void reset_signals() {
  /*ADD_CAN_SIGNAL(CEM_2, Engine_Speed,         0); //
  ADD_CAN_SIGNAL(CEM_2, Engine_Speed_UB,      0); //
  ADD_CAN_SIGNAL(CEM_2, Engine_Speed_QF,      0); //
  ADD_CAN_SIGNAL(CEM_2, Vehicle_Speed,        0); //
  ADD_CAN_SIGNAL(CEM_2, Vehicle_Speed_UB,     0); //
  ADD_CAN_SIGNAL(CEM_2, Vehicle_Speed_QF,     0); //
  ADD_CAN_SIGNAL(CEM_2, Fuel_Sensor,          0); //
  ADD_CAN_SIGNAL(CEM_2, Fuel_Sensor_UB,       0); //
  ADD_CAN_SIGNAL(CEM_2, Fuel_Sensor_QF,       0); //*/

  r_or_l = 0;
  b = false;
  w = 3;
  ADD_CAN_SIGNAL(CEM_3, Direction_Indication,     0); // No blinker
  ADD_CAN_SIGNAL(CEM_3, Direction_Indication_UB,  1); // No change
  ADD_CAN_SIGNAL(CEM_3, Main_Beam_Indication,     0); // Off
  ADD_CAN_SIGNAL(CEM_3, Main_Beam_Indication_UB,  1); // no change
  ADD_CAN_SIGNAL(CEM_3, ABS_Warning_Lamp,         3); // Off
  ADD_CAN_SIGNAL(CEM_3, ABS_Warning_Lamp_UB,      1); // No Change
  ADD_CAN_SIGNAL(CEM_3, Read_Message,             0); // Do nothing

}

void setup()
{
  Serial.begin(115200);
  Serial.println("Initializing ... weyoh");

  /* Set up SPI Communication */
  SPI.setClockDivider(SPI_CLOCK_DIV128);
  SPI.setDataMode(SPI_MODE0);
  SPI.setBitOrder(MSBFIRST);
  SPI.begin();


  /* 125 kbit/s CAN bus, 8 MHz clock to CAN controller */
  if(CAN_EDU.Init(125, 8))
    Serial.println("CAN Node is Ready ...");
  else
  {
    Serial.println("MCP2515 Init Failed ...");
    while(true) {
    }
  }

  CAN_EDU.InitFilters(true);


  ADD_CAN_SIGNAL(CEM_1, Power_Mode,           6); //
  ADD_CAN_SIGNAL(CEM_1, Power_Mode_UB,        1); //
  ADD_CAN_SIGNAL(CEM_1, Power_Mode_QF,        3); //

  reset_signals();
}


//Read input from the keyboard to the serial monitor.
/*
Add all the necessary commands here
and do the same for all the cases below.

You want to turn the different signals on/off
with the same buttonpress, remember to check
if a light is on or off before deciding which
of the two the user wants to do.

Remember that in order for the blinkers to
work, you have to send several on and off
commands after each other with a delay between.
The commands can only tell the blinker to turn
on and off, not to blink. That is up to the code.

Unlike the other signals, you have to send
each on/off command immediatly after adding
the signal. If you add a turn off signal,
you have to send it. If you send a turn on signal,
you have to send it.
*/

void loop() {
  // Handle interrupts

  if(digitalRead(CAN_EDU_NOT_INT_PIN) == 0)
    CAN_EDU.intHandler();

  if(Serial.available() != 0) {
    cmd = Serial.read();
    Serial.print("input: ");

    switch(cmd){
      case 's': // POWER ON!
        Serial.println("Power on");

        ADD_CAN_SIGNAL(CEM_1, Power_Mode, 6);
        ADD_CAN_SIGNAL(CEM_1, Power_Mode_UB, 1);
        ADD_CAN_SIGNAL(CEM_1, Power_Mode_QF, 3);

        break;
      case 'e': //POWER OFF
        Serial.println("Power off");
        ADD_CAN_SIGNAL(CEM_1, Power_Mode, 0);
        ADD_CAN_SIGNAL(CEM_1, Power_Mode_UB, 1);
        ADD_CAN_SIGNAL(CEM_1, Power_Mode_QF, 3);
        break;
      case 'l':
        Serial.println("Direction Left");
        r_or_l = (r_or_l == 1) ? 0 : 1;
        break;
      case 'r':
        Serial.println("Direction Right");
        r_or_l = (r_or_l == 2) ? 0 : 2;
        break;
      case 'b':
          b = !b;
          Serial.print("Beam is ");
          Serial.println(b);
          ADD_CAN_SIGNAL(CEM_3, Main_Beam_Indication, b);
          ADD_CAN_SIGNAL(CEM_3, Main_Beam_Indication_UB, 1);
        break;
      case 'w':
          w = (w==3) ? 1 : 3;
          Serial.println("ABS");
          ADD_CAN_SIGNAL(CEM_3, ABS_Warning_Lamp, w);
          ADD_CAN_SIGNAL(CEM_3, ABS_Warning_Lamp_UB, 1);
        break;
      case 't':
        Serial.println("Reset signals");
        reset_signals();
        break;
      case 'c':
      Serial.println("Next message");
        ADD_CAN_SIGNAL(CEM_3, Read_Message, 1);
        break;
      case '1':
        Serial.print("Speed: ");
        Serial.println(speed);
      case 'g':
        Serial.print("gear: ");
        Serial.println(gear);
      }
  }

  // Read sped
  speed = analogRead(POTI_PIN)*26;

  // Calculate rpm
  rpm = speed/100 * speedToRPM[gear-1];

  // Shift gear?
  if (rpm<500 && gear > 1) {
    gear = gear -1;
    rpm = speed/100 * speedToRPM[gear-1];
  } else if (rpm>2500 && gear < 6){
    gear = gear +1;
    rpm = speed/100 * speedToRPM[gear-1];
  }

  // Calculate rpm


  ADD_CAN_SIGNAL(CEM_2, Vehicle_Speed, speed);
  ADD_CAN_SIGNAL(CEM_2, Vehicle_Speed_UB, 1);
  ADD_CAN_SIGNAL(CEM_2, Vehicle_Speed_QF, 3);

  ADD_CAN_SIGNAL(CEM_2, Engine_Speed, rpm);
  ADD_CAN_SIGNAL(CEM_2, Engine_Speed_UB, 1);
  ADD_CAN_SIGNAL(CEM_2, Engine_Speed_QF, 3);

  ADD_CAN_SIGNAL(CEM_2, Fuel_Sensor, 500);
  ADD_CAN_SIGNAL(CEM_2, Fuel_Sensor_UB, 1);
  ADD_CAN_SIGNAL(CEM_2, Fuel_Sensor_QF, 3);

  ON_TIMER(50, cem1_send() );
  ON_TIMER(70, cem2_send() );
  ON_TIMER(100, cem3_send() );


  /* You can use the ON_TIMER command to send
  your messages on a regular interval.
  Example:
    ON_TIMER(20, SEND_CAN_MSG(CEM_1));

    Which sends the SEND_CAN_MSG(CEM1) command
    every 20ms.

  For the blinkers, remember that they have to
  blink. You have to turn the lights on and off
  at a rate of your choosing. You can use the
  delay(amount of time in ms) function.
  */

}



/******************************************************************************
 * void can_EDU_receive()
 *
 *  Handels the received Data frames on the CAN_EDU Bus
 *
 * Arguments: none
 * Return value: none
 *****************************************************************************/
void can_EDU_receive()
{

        uint16_t temp;
	Frame can_frame;

	if(CAN_EDU.GetRXFrame(can_frame))
	{
                  if(can_frame.id==301){
                   // do something
                   }

	}

}

/******************************************************************************
 * uint16_t unpack_frame(uint8_t *message, uint8_t startBit, uint8_t bitLength, uint8_t Motorola_Intel){
 *
 *  extracts a certan Signal of a received CAN Frame
 *
 * Arguments:   message - data array of the can frame (e.g. can_frame.data)
 *              startBit - Start Bit of the Signal
 *              bitLength - Length in Bit of the Signal
 *              Motorola_Intel - Bit Order of the signal (0 Motorola / 1 Intel)
 * Return value: extracted Signal
 *****************************************************************************/
uint16_t unpack_frame(uint8_t *message, uint8_t startBit, uint8_t bitLength, uint8_t Motorola_Intel){
  uint8_t bitPosition;
  uint16_t temp=0;
  uint8_t byteN,bitN;
  boolean  bitToWrite;
  uint8_t n=0;


      for(int i=0;i<bitLength;i++){
        //get position of the bit in the message
        if(Motorola_Intel==1){ //Motorola_Intel=1 => Intel
          bitPosition = startBit + i;
          byteN = bitPosition/8;
          bitN = bitPosition%8;
        }
        else{
          bitPosition = startBit + i - n*16;
          bitN = bitPosition%8;
          byteN = bitPosition/8;
          if(bitN==7){
               n++;  //if overrun the current byte go one byte lower
          }
        }

        bitToWrite=(message[byteN] >> bitN) & 1;

        temp=temp + (bitToWrite << i);
      }
return temp;

}



/******************************************************************************
 *  int add_signal(uint8_t *message,int16_t value,uint8_t startBit, uint8_t bitLength, uint8_t Motorola_Intel)
 *
 *  Adds a Signal to a CAN message
 *
 * Arguments:   message - data array in which the the CAN message is stored
 *              value - value of the Signal to add
 *              startBit - Start Bit of the Signal
 *              bitLength - Length in Bit of the Signal
 *              Motorola_Intel - Bit Order of the signal (0 Motorola / 1 Intel)
 * Return value: 0 if signal is sucessfully added to the message / 1 if the signal does  not fit into the message
 *****************************************************************************/
int add_signal(uint8_t *message,int16_t value,uint8_t startBit, uint8_t bitLength, uint8_t Motorola_Intel){
uint8_t bitPosition;
uint8_t temp;
uint8_t byteN,bitN;
boolean  bitToWrite;
uint8_t n=0;


if( ( (startBit + bitLength < 64) & Motorola_Intel==1 ) || ( ( (startBit/8)*8 + (8 - startBit%8 )  >= bitLength ) & Motorola_Intel==0 )){  // check if signal fits into message
      for(int i=0;i<bitLength;i++){
        bitToWrite=(value >> i) & 1; //get bit

        //get position of the bit in the message
        if(Motorola_Intel==1){ //Motorola_Intel=1 => Intel
          bitPosition = startBit + i;
          byteN = bitPosition/8;
          bitN = bitPosition%8;
        }
        else{
          bitPosition = startBit + i - n*16;
          bitN = bitPosition%8;
          byteN = bitPosition/8;
          if(bitN==7){
               n++;  //if overrun the current byte go one byte lower
          }
        }
        //write the current bit in the message
        message[byteN] = (message[byteN] & ~(1 << bitN)) | (bitToWrite << bitN);
      }
}
else{
      return 0;      // return 0 if signal is to large
}

     return 1;       // return 1 if signal is succesfully added to message
}

/******************************************************************************
 * void send_CAN_msg(uint16_t SID,uint8_t *message,uint8_t DLC)
 *
 *  Sends Can message on the bus
 *
 * Arguments:   SID - short id of the message
 *              message - data array of the messages
 *              DLC - Data length
 *
 * Return value: none
 *****************************************************************************/
void send_CAN_msg(uint16_t SID,uint8_t *message,uint8_t DLC)
{
  Frame can_frame;


  can_frame.id = SID;   // EID if ide set, SID otherwise
  can_frame.srr = 0;      // Standard Frame Remote Transmit Request (in extenden mode recesive (1) )
  can_frame.rtr = 0;      // Remote Transmission Request
  can_frame.ide = 0;      // Extended ID flag / dominant (0) to indicate SID
  can_frame.dlc = DLC;      // Number of data bytes
  for(int i=0;i<DLC;i++){
    can_frame.data[i] = message[i];    // little endian
  }
  CAN_EDU.EnqueueTX(can_frame);
  /*Serial.print("Sent CAN message: ");
  for(int i=0;i<DLC;i++){
    Serial.print(can_frame.data[i]);
    Serial.print(" ");
  }
  Serial.println(" ");*/
}

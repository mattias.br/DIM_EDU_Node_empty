#ifndef __TIMER_H__
#define __TIMER_H__

/******************************************************************************
 * The ON_TIMER(interval, function) macro is used to run functions continuously
 * at specific time intervals. These functions (and all other code) must be
 * non-blocking and the worst case combined running time must be less than the
 * shortest interval to ensure timing. It should be called from the
 * loop()-function.
 *
 * Example usage:
 *
 * void loop()
 * {
 *   ON_TIMER(20, send_CEM1_frame());        // Every 20 ms
 *   ON_TIMER(1000, Serial.println("Ping")); // Every 1 second
 * }
 *
 *****************************************************************************/

#define ON_TIMER(x, y)                               \
	{                                                \
		static uint32_t __timer_timeout__ = 0;       \
	                                                 \
		if(millis() >= __timer_timeout__)            \
		{                                            \
			__timer_timeout__ += (x);                \
			y;                                       \
		}                                            \
	} do {} while(0)

#endif


#define RESET_TEMP_CAN_MSG()     memset(CANmessage, 0, sizeof(CANmessage))
#define ADD_CAN_SIGNAL(nodeName,signalName,value)    add_signal(CAN_## nodeName ##_Internal_message,value,dbc_## nodeName ##_signals[DBC_MSG_## nodeName ##_SIGNAL_## signalName ].start_bit,dbc_## nodeName ##_signals[DBC_MSG_## nodeName ##_SIGNAL_## signalName ].len,DBC_MOTOROLA)
#define SEND_CAN_MSG(nodeName)  send_CAN_msg(dbc_## nodeName ##_message.id,CAN_## nodeName ##_Internal_message, dbc_## nodeName ##_message.dlc)
#define UNPACK_CAN_FRAME(frameData,nodeName,signalName)    unpack_frame(frameData,  dbc_## nodeName ##_signals[DBC_MSG_## nodeName ##_SIGNAL_## signalName ].start_bit,  dbc_## nodeName ##_signals[DBC_MSG_## nodeName ##_SIGNAL_## signalName ].len,  DBC_MOTOROLA)


#define USE_CEM_1
#define USE_CEM_2
#define USE_CEM_3




#ifdef USE_CEM_1
uint8_t CAN_CEM_1_Internal_message[8] = {0};
#endif


#ifdef USE_CEM_2
uint8_t CAN_CEM_2_Internal_message[8] = {0};
#endif


#ifdef USE_CEM_3
uint8_t CAN_CEM_3_Internal_message[8] = {0};
#endif


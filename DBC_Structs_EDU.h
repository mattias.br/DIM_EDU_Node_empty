#ifndef __0f5e286b_ad72_488b_af79_a472a9758b65__
#define __0f5e286b_ad72_488b_af79_a472a9758b65__

struct dbc_signal
{
	const char *name;
	int start_bit, len;
	int byte_order;
	float a, b;
	float min, max;
};

struct dbc_msg
{
	const char *name;
	int id;
	int dlc;
};

#define DBC_MOTOROLA	0
#define DBC_INTEL		1

#define DBC_MSG_CEM_1_SIGNAL_Power_Mode	0
#define DBC_MSG_CEM_1_SIGNAL_Power_Mode_UB	1
#define DBC_MSG_CEM_1_SIGNAL_Power_Mode_QF	2

const struct dbc_signal dbc_CEM_1_signals [] =
{
	{
		.name = "Power_Mode",
		.start_bit = 2,
		.len = 5,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 0.0
	},
	{
		.name = "Power_Mode_UB",
		.start_bit = 7,
		.len = 1,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 1.0
	},
	{
		.name = "Power_Mode_QF",
		.start_bit = 0,
		.len = 2,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 0.0
	}
};

const struct dbc_msg dbc_CEM_1_message =
{
	.name = "CEM_1",
	.id = 0x12d,
	//.id = 0x301,
	.dlc = 8
};

#define DBC_MSG_CEM_2_SIGNAL_Engine_Speed	0
#define DBC_MSG_CEM_2_SIGNAL_Engine_Speed_UB	1
#define DBC_MSG_CEM_2_SIGNAL_Engine_Speed_QF	2
#define DBC_MSG_CEM_2_SIGNAL_Vehicle_Speed	3
#define DBC_MSG_CEM_2_SIGNAL_Vehicle_Speed_UB	4
#define DBC_MSG_CEM_2_SIGNAL_Vehicle_Speed_QF	5
#define DBC_MSG_CEM_2_SIGNAL_Fuel_Sensor	6
#define DBC_MSG_CEM_2_SIGNAL_Fuel_Sensor_UB	7
#define DBC_MSG_CEM_2_SIGNAL_Fuel_Sensor_QF	8

const struct dbc_signal dbc_CEM_2_signals [] =
{
	{
		.name = "Engine_Speed",
		.start_bit = 26,
		.len = 13,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 8191.0
	},
	{
		.name = "Engine_Speed_UB",
		.start_bit = 23,
		.len = 1,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 1.0
	},
	{
		.name = "Engine_Speed_QF",
		.start_bit = 24,
		.len = 2,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 0.0
	},
	{
		.name = "Vehicle_Speed",
		.start_bit = 55,
		.len = 16,
		.byte_order = 0,
		.a = 0.009999999776482582,
		.b = 0.0,
		.min = 0.0,
		.max = 310.0
	},
	{
		.name = "Vehicle_Speed_UB",
		.start_bit = 39,
		.len = 1,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 1.0
	},
	{
		.name = "Vehicle_Speed_QF",
		.start_bit = 53,
		.len = 2,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 0.0
	},
	{
		.name = "Fuel_Sensor",
		.start_bit = 58,
		.len = 10,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 1022.0
	},
	{
		.name = "Fuel_Sensor_UB",
		.start_bit = 52,
		.len = 1,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 1.0
	},
	{
		.name = "Fuel_Sensor_QF",
		.start_bit = 56,
		.len = 2,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 0.0
	}
};

const struct dbc_msg dbc_CEM_2_message =
{
	.name = "CEM_2",
	.id = 0x12e,
	.dlc = 8
};

#define DBC_MSG_CEM_3_SIGNAL_Main_Beam_Indication	0
#define DBC_MSG_CEM_3_SIGNAL_Main_Beam_Indication_UB	1
#define DBC_MSG_CEM_3_SIGNAL_Direction_Indication	2
#define DBC_MSG_CEM_3_SIGNAL_Direction_Indication_UB	3
#define DBC_MSG_CEM_3_SIGNAL_ABS_Warning_Lamp	4
#define DBC_MSG_CEM_3_SIGNAL_ABS_Warning_Lamp_UB	5
#define DBC_MSG_CEM_3_SIGNAL_Read_Message	6

const struct dbc_signal dbc_CEM_3_signals [] =
{
	{
		.name = "Main_Beam_Indication",
		.start_bit = 9,
		.len = 1,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 0.0
	},
	{
		.name = "Main_Beam_Indication_UB",
		.start_bit = 10,
		.len = 1,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 1.0
	},
	{
		.name = "Direction_Indication",
		.start_bit = 11,
		.len = 2,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 0.0
	},
	{
		.name = "Direction_Indication_UB",
		.start_bit = 13,
		.len = 1,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 1.0
	},
	{
		.name = "ABS_Warning_Lamp",
		.start_bit = 14,
		.len = 2,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 0.0
	},
	{
		.name = "ABS_Warning_Lamp_UB",
		.start_bit = 6,
		.len = 1,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 1.0
	},
	{
		.name = "Read_Message",
		.start_bit = 8,
		.len = 1,
		.byte_order = 0,
		.a = 1.0,
		.b = 0.0,
		.min = 0.0,
		.max = 0.0
	}
};

const struct dbc_msg dbc_CEM_3_message =
{
	.name = "CEM_3",
	.id = 0x12f,
	.dlc = 8
};

#endif
